namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updated : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Books", "Category_CategoryId", "dbo.Categories");
            DropIndex("dbo.Books", new[] { "Category_CategoryId" });
            CreateTable(
                "dbo.CategoriesBooks",
                c => new
                    {
                        Categories_CategoryId = c.Int(nullable: false),
                        Books_BookId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Categories_CategoryId, t.Books_BookId })
                .ForeignKey("dbo.Categories", t => t.Categories_CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Books", t => t.Books_BookId, cascadeDelete: true)
                .Index(t => t.Categories_CategoryId)
                .Index(t => t.Books_BookId);
            
            DropColumn("dbo.Books", "Category_CategoryId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Books", "Category_CategoryId", c => c.Int());
            DropForeignKey("dbo.CategoriesBooks", "Books_BookId", "dbo.Books");
            DropForeignKey("dbo.CategoriesBooks", "Categories_CategoryId", "dbo.Categories");
            DropIndex("dbo.CategoriesBooks", new[] { "Books_BookId" });
            DropIndex("dbo.CategoriesBooks", new[] { "Categories_CategoryId" });
            DropTable("dbo.CategoriesBooks");
            CreateIndex("dbo.Books", "Category_CategoryId");
            AddForeignKey("dbo.Books", "Category_CategoryId", "dbo.Categories", "CategoryId");
        }
    }
}
