﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using BookStore.Models;
using System.Web.Mvc;

namespace BookStore.Controllers
{
    public class CartController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Cart
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult OrderNow(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (Session["cart"] == null)
            {
                List<ShoppingCartItem> cartlist = new List<ShoppingCartItem>
                {
                    new ShoppingCartItem(db.Books.Find(id))
                };
                Session["cart"] = cartlist;
            }
            else
            {
                List<ShoppingCartItem> cartlist = (List<ShoppingCartItem> ) Session["cart"];
                cartlist.Add(new ShoppingCartItem(db.Books.Find(id)));
                Session["cart"] = cartlist;
            }

            return View("Index");
        }
    }
}