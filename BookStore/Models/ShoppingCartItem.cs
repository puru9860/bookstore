﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Models
{
    public class ShoppingCartItem
    {
        public string ShoppingCartId { get; set; }
        public int ShoppingCartItemId { get; set; }
        public Books Book { get; set; }
        public decimal Amount { get; set; }

        public ShoppingCartItem(Books book)
        {
            this.Book = book;
        }
    }
}
