﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using System.Linq;
using System.Web;
using System.Threading.Tasks;


namespace BookStore.Models
{
    public class Books
    {
      
        [Key]
        public int BookId { get; set; }

        [StringLength(100, MinimumLength = 2)]   
        [RegularExpression("([a-zA-Z0-9 .&'-]+)", ErrorMessage = "The field Name should only include letters and number.")]
        [DataType(DataType.Text)]
        [Required]
        public string Name { get; set; }

        [Range(0, 10000)]
        [DataType(DataType.Currency)]
        [Required]
        public decimal Price { get; set; }

        [StringLength(255, MinimumLength = 2)]
        [DataType(DataType.MultilineText)]
        [Required]
        public string Description { get; set; }

        [DataType(DataType.ImageUrl)]
        public string ImageUrl { get; set; }

        [NotMapped]
        public HttpPostedFileBase ImageFile { get; set; }

        [DisplayName("Select Category")]
        public List<int> CategoriesId { get; set; }

        public virtual ICollection<Categories> Category { get; set; }

        public Books()
        {
            Category = new HashSet<Categories>();
        }



    }
}